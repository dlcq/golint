package issue

import "go/token"

type Level int

const (
	Tips Level = iota
	Warn
	Error
)

type Issue interface {
	Name() string
	FilePath() string
	Pos() token.Position
	Description() string
	FixPlan() (bool, string)
	Level() Level
}

type issue struct {
	name    string
	pos     token.Position
	desc    string
	fixPlan string
	level   Level
}

// NewSimpleIssue create a new simple issue.
func NewSimpleIssue(name string, level Level, pos token.Position, desc, fixPlan string) Issue {
	return &issue{
		name:    name,
		pos:     pos,
		desc:    desc,
		fixPlan: fixPlan,
		level:   level,
	}
}

// Name return the name of issue.
func (i *issue) Name() string {
	return i.name
}

// FilePath return the path of the issued file.
func (i *issue) FilePath() string {
	return i.pos.Filename
}

// Pos return the position of the issue.
func (i *issue) Pos() token.Position {
	return i.pos
}

// Description return the description of the issue.
func (i *issue) Description() string {
	return i.desc
}

// FixPlan return the fix plan of the issue, if any.
func (i *issue) FixPlan() (bool, string) {
	return len(i.fixPlan) > 0, i.fixPlan
}

// Level return the level of the issue, default is Tips.
func (i *issue) Level() Level {
	return i.level
}

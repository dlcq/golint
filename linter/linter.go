package linter

import (
	"context"
	"golang.org/x/tools/go/packages"
	"golint/issue"
	"golint/report"
)

// Linter is the core of this project.
// they analysis Go code and report issues
type Linter interface {
	Name() string
	WithBeforeRun(func() error) Linter
	Run(context.Context, ...*packages.Package) (report.Report, error)
	WithAfterRun(func(...issue.Issue) error) Linter
	Close()
	LoadMode() packages.LoadMode
}

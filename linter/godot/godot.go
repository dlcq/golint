package godot

import (
	"context"
	"fmt"
	"github.com/tetafro/godot"
	"golang.org/x/tools/go/packages"
	"golint/issue"
	"golint/linter"
	"golint/report"
)

const name = "godot"

type dotLinter struct {
	mode   packages.LoadMode
	before []func() error
	after  []func(...issue.Issue) error
}

// NewLinter create a new dot linter.
func NewLinter() linter.Linter {
	return &dotLinter{mode: packages.NeedSyntax | packages.NeedTypes | packages.NeedName | packages.NeedImports}
}

// Name return the name of dot linter.
func (l *dotLinter) Name() string {
	return name
}

// WithBeforeRun attach a func to linter,
// the cb function will be executed before Run.
func (l *dotLinter) WithBeforeRun(cb func() error) linter.Linter {
	l.before = append(l.before, cb)
	return l
}

// Run start analysis code.
func (l *dotLinter) Run(ctx context.Context, pkgs ...*packages.Package) (report.Report, error) {
	for _, cb := range l.before {
		if err := cb(); err != nil {
			return nil, fmt.Errorf("dotLinter before run err: %w", err)
		}
	}

	settings := godot.Settings{
		CheckAll: false,
	}

	issues := make([]issue.Issue, 0)
	for _, pkg := range pkgs {
		messages := make([]godot.Message, 0)
		for _, file := range pkg.Syntax {
			messages = append(messages, godot.Run(file, pkg.Fset, settings)...)
		}

		for _, message := range messages {
			issues = append(issues, issue.NewSimpleIssue(pkg.Name, issue.Tips, message.Pos, message.Message, ""))
		}

		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}
	}

	for _, cb := range l.after {
		if err := cb(issues...); err != nil {
			return nil, fmt.Errorf("dotLinter after run err: %w", err)
		}
	}

	return report.NewSimpleReport(l.Name(), issues...), nil
}

// WithAfterRun attach a func to linter,
// the cb function will be executed after Run.
func (l *dotLinter) WithAfterRun(cb func(...issue.Issue) error) linter.Linter {
	l.after = append(l.after, cb)
	return l
}

// Close close the dot linter,
// do nothing.
func (l *dotLinter) Close() {
}

// LoadMode return the ast Mode.
func (l *dotLinter) LoadMode() packages.LoadMode {
	return l.mode
}

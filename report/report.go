package report

import "golint/issue"

// Report is the result of linters.
type Report interface {
	Name() string
	Issues() []issue.Issue
}

type simpleReport struct {
	name   string
	issues []issue.Issue
}

// Create a new simple report.
func NewSimpleReport(name string, issues ...issue.Issue) Report {
	return &simpleReport{name: name, issues: issues}
}

// Name return the name of a report.
func (sp *simpleReport) Name() string {
	return sp.name
}

// Issues return issues of the report.
func (sp *simpleReport) Issues() []issue.Issue {
	return sp.issues
}

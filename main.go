package main

import (
	"flag"
	"golint/executor"
	"golint/linter/godot"
	"log"
)

var (
	path = flag.String("d", "", "Path to the project code.")
)

//go:generate go run main.go -d ./...
func main() {
	flag.Parse()

	exec := executor.NewSimpleExecutor(*path)
	defer exec.Close()

	exec.AddLinters(
		godot.NewLinter(),
	)

	if err := exec.Execute(); err != nil {
		log.Fatalf("Execute() failed with err: %s", err.Error())
	}

	if err := exec.Render(); err != nil {
		log.Fatalf("Render() failed with err: %s", err.Error())
	}
}

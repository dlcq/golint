package load

import (
	"context"
	"fmt"
	"golang.org/x/tools/go/packages"
	"log"
	"os"
	"strings"
	"sync"
)

// PackageLoader load packages.
type PackageLoader interface {
	Load(context.Context, string, packages.LoadMode) ([]*packages.Package, error)
}

var defaultLoader PackageLoader = &cachePackageLoader{}

type cachePackageLoader struct {
	cache sync.Map
}

// Load load packages in path.
func (l *cachePackageLoader) Load(ctx context.Context, path string, mode packages.LoadMode) ([]*packages.Package, error) {
	cacheKey := fmt.Sprintf("path-%s:mode-%d", path, mode)
	if cache, ok := l.cache.Load(cacheKey); ok {
		return cache.([]*packages.Package), nil
	}

	if len(path) == 0 {
		path = "./..."
	}
	conf := &packages.Config{
		Mode:    mode,
		Context: ctx,
		Logf:    log.Printf,
		Env:     os.Environ(),
	}
	pkgs, err := packages.Load(conf, path)
	if err != nil {
		return nil, err
	}
	for _, pkg := range pkgs {
		for _, e := range pkg.Errors {
			if strings.Contains(e.Msg, "no Go files") {
				return nil, fmt.Errorf("path: %s, err: %s", pkg.PkgPath, e.Msg)
			}
			if strings.Contains(e.Msg, "cannot find package") {
				return nil, fmt.Errorf("path: %s, err: %s", pkg.PkgPath, e.Msg)
			}
		}
	}
	l.cache.Store(cacheKey, pkgs)
	return pkgs, nil
}

// Load load packages in path with default loader.
func Load(ctx context.Context, path string, mode packages.LoadMode) ([]*packages.Package, error) {
	return defaultLoader.Load(ctx, path, mode)
}

// SetLoader reset default loader with user defined loader.
func SetLoader(loader PackageLoader) {
	defaultLoader = loader
}

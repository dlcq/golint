module golint

go 1.14

require (
	github.com/tetafro/godot v0.2.5
	golang.org/x/tools v0.0.0-20200422205258-72e4a01eba43
)

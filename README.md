# Golint
_A static code analysis tool for Go_
### Inspired by
- [golangci-lint](https://github.com/golangci/golangci-lint)
- [goreporter](https://github.com/360EntSecGroup-Skylar/goreporter)
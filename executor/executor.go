package executor

import (
	"context"
	"fmt"
	"golang.org/x/tools/go/packages"
	"golint/linter"
	"golint/report"
	"log"
	"os"
	"sync"
)

// Executor execute linters and generate reports.
// executor is the runner of linters
type Executor interface {
	AddLinters(...linter.Linter)
	Execute() error
	Render() error
	Close()
}

// NewSimpleExecutor create a new simple executor.
func NewSimpleExecutor(path string) Executor {
	return &executor{
		root:   path,
	}
}

type executor struct {
	root    string
	linters []linter.Linter
	ch      chan error
	reports sync.Map
}

// AddLinters append linters to executor.
func (e *executor) AddLinters(linters ...linter.Linter) {
	e.linters = append(e.linters, linters...)
}

// Execute execute linters async.
func (e *executor) Execute() error {
	if e.ch == nil {
		e.ch = make(chan error, len(e.linters))
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var mode packages.LoadMode
	for _, l := range e.linters {
		mode |= l.LoadMode()
	}

	conf := &packages.Config{
		Mode:    mode,
		Context: ctx,
		Logf:    log.Printf,
		Env:     os.Environ(),
	}
	pkgs, err := packages.Load(conf, e.root)
	if err != nil {
		return fmt.Errorf("load packages failed with err: %w", err)
	}

	for _, l := range e.linters {
		go func(l linter.Linter) {
			r, err := l.Run(ctx, pkgs...)
			if err == nil {
				e.reports.Store(l.Name(), r)
			}
			e.ch <- err
		}(l)
	}

	for i := 0; i < len(e.linters); i++ {
		err := <-e.ch
		if err != nil {
			return err
		}
	}

	return nil
}

// Render output reports to console.
func (e *executor) Render() error {
	e.reports.Range(func(key, value interface{}) bool {
		r := value.(report.Report)
		for _, issue := range r.Issues() {
			fmt.Printf("Linter: %s %s:%d:%d issue: %s\n", r.Name(), issue.FilePath(), issue.Pos().Line,
				issue.Pos().Column, issue.Description())
		}

		return true
	})

	return nil
}

// Close close executor and close channel.
func (e *executor) Close() {
	close(e.ch)
}
